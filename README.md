# go-cobra-goreleaser-version-example

A sample repo to go alongside [_Getting a `--version` flag for Cobra CLIs in Go, built with GoReleaser_](https://www.jvt.me/posts/2023/02/27/go-cobra-goreleaser-version/).
