package main

import "gitlab.com/tanna.dev/go-cobra-goreleaser-version-example/cmd"

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	cmd.SetVersionInfo(version, commit, date)
	cmd.Execute()
}
